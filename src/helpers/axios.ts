import axios from 'axios';

export const zabApi = axios.create({
	baseURL: process.env.VUE_APP_API_URL || "http://localhost:3000",
	withCredentials: true,
	headers: {
		'Access-Control-Allow-Origin': '*'
	}
});