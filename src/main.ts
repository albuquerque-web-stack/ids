import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
// @ts-ignore
import store from './store';
import '@materializecss/materialize';
import '@materializecss/materialize/dist/css/materialize.min.css';

createApp(App).use(store).use(router).mount('#app');
