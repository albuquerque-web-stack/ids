import { Commit } from 'vuex';

export default {
	namespaced: true,
	state: {
		timestamp: null
	},
	mutations: {
		SET_TIMESTAMP(state: Record<string, any>, stamp: number): void {
			state.timestamp = stamp;
		}
	},
	actions: {
		setTimestamp({ commit }: { commit: Commit }, amount: number): void {
			commit('SET_TIMESTAMP', amount);
		}
	},
	getters: {
		getTimestamp(state: Record<string, any>): number {
			return state.timestamp;
		}
	}
}