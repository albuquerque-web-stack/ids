// @ts-ignore
import M from '@materializecss/materialize';
import { zabApi } from '@/helpers/axios';
import router from '../router/index';
import { Commit } from 'vuex';

export default {
	namespaced: true,
	state: {
		user: {
			data: null,
			isLoggedIn: false
		}
	},
	mutations: {
		SET_USER(state: Record<string, any>, user: Record<string, any>): void {
			state.user.data = user;
		},
		SET_LOGGED_IN(state: Record<string, any>, loggedIn: boolean): void {
			state.user.isLoggedIn = loggedIn;
		},
	},
	actions: {
		setData: async ({ commit }: { commit: Commit }, userData: Record<string, any>): Promise<void> => {
			if(userData) {
				commit('SET_USER', userData);
				commit('SET_LOGGED_IN', true);
				return;
			}
		},
		getData: async({ commit }: { commit: Commit }, token: string): Promise<void> => {
			try {
				if((localStorage.getItem('guest') === 'true' && localStorage.getItem('ids_token') !== '') || (localStorage.getItem('guest') !== 'true' && localStorage.getItem('ids_token') !== undefined)) {
					const { data } = await zabApi.post('/ids/checktoken', {
						token: token
					});
					if(data.ret_det.code === 200) {
						commit('SET_USER', data.data);
						commit('SET_LOGGED_IN', true);
						localStorage.setItem('guest', 'false')
						router.push('/home');
						return;
					} else {
						commit('SET_USER', null);
						commit('SET_LOGGED_IN', false);
						M.toast({
							html: `<i class="material-icons left">error_outline</i> ${data.ret_det.message} <div class="border"></div>`,
							displayLength: 5000,
							classes: 'toast toast_error'
						});
						router.push('/');
					}
				}

				return;
			} catch(e) {
				console.log(e);
			}
		}
	},
	getters: {
		getUserData(state: Record<string, any>): Record<string, any> {
			return state.user.data;
		}
	}
};