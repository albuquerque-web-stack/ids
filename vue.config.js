module.exports = {
	pluginOptions: {
		electronBuilder: {
			preload: 'src/preload.js',
			builderOptions: {
				"appId": "org.zabartcc.ids",
				"productName": "Albuquerque ARTCC IDS",
				"artifactName": "AlbuquerqueIDSInstall.${ext}",
				"win": {
					"publish": {
						"provider": "spaces",
						"name": "zabartcc",
						"region": "sfo3",
						"channel": "latest",
						"path": "/ids"
					}
				}
			},
		}
	}
}